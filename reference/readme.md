# How I started mining FLO coins using my GPU  

To get started with GPU mining, Create an account with Suprnova.  
After registration, navigate to My Account and then click on Edit Account.  
Scroll down and update the payment address by entering your Flo Core wallet address and make sure you click on the update button.  
(Use the payment address that you got when you registered for FLO Core Wallet.  
You can find your Payment address by navigating to the receive section and clicking on the request payment option).  

Once you’ve updated the payment option, Go to my Account and click on My Workers.  
Add a new worker and update the credentials accordingly.  
Again make sure you click on the update button once you’ve added a new worker.  
Make sure that the monitor option is enabled.  

Now once you’ve got all the Supernova stuff figured out, it’s time to get the miner running.  
I’ve given the links to all the resources below. From there, Download GC Miner. You’ll need an application to extract the RAR file.  
I used Peazip, you. can use your preferred application. Once you’ve got it downloaded, extract the file and make sure you have it  
stored properly and safely in a specific new folder.  

After it’s done, open the extracted file and navigate to the config file.  
Delete the config file. Once you’re done deleting it, click on the empty space present in the address  
bar and type cmd. This will open the command prompt. Now to start running the mining software executethe command:-  

```
cgminer — scrypt -o stratum+tcp://flo.suprnova.cc:3210 -u [workername] -p [worker_pass]  
```  

Here “workername” and “worker_pass” refer to the credentials of the workers which we created in Supernova.  
Note that the workername should be typed in the format [your username].[worker name] and replace it accordingly.  
for example  

```
-u bharat01.mall01 -p duckgostack123  
```  

Once you’ve executed the program by replacing the appropriate usernames and passwords, the process of mining will be started. and this is what it’ll look like.  

To quit the program press q. We can’t always type in a long command so let’s create a shortcut. Create a new text file in the folder and rename it as start.bat  
edit this document in the text file and type in the same executable that we used to start the mining process.  

```
cgminer — scrypt -o stratum+tcp://flo.suprnova.cc:3210 -u [workername] -p [worker_pass]
```  

After saving the bat file, copy and paste the start.bat file to the desktop for easy access.  

Happy mining!!  

All the resources used:-  

#### GC Miner:-  
(https://drive.google.com/file/d/1nNutlChYfkXFoDIE_bFVtH9YhVGWPdU9/view?usp=drive_link)  
#### Peazip download:-  
(https://peazip.github.io/index.html)  
#### Suprnova:-  
(https://flo.suprnova.cc/index.php)  

### Written by Bhardwaj Hariharan on Medium  
For attached screenshots, read this Medium post  
(https://medium.com/@bhardhwaj01/how-i-started-mining-flo-coins-using-my-gpu-01ea0ac436eb)
